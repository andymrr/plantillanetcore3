#pragma checksum "E:\Proyectos\Plantilla Administrativa NetCore 3\PlantillaAdminNetcore3\PlantillaAdminNetcore3\Views\Home\Login.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc1c958ab8e1cf91a72db2e0a9f9c6d22ca356ae"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Login), @"mvc.1.0.view", @"/Views/Home/Login.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Proyectos\Plantilla Administrativa NetCore 3\PlantillaAdminNetcore3\PlantillaAdminNetcore3\Views\_ViewImports.cshtml"
using PlantillaAdminNetcore3;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\Proyectos\Plantilla Administrativa NetCore 3\PlantillaAdminNetcore3\PlantillaAdminNetcore3\Views\_ViewImports.cshtml"
using PlantillaAdminNetcore3.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc1c958ab8e1cf91a72db2e0a9f9c6d22ca356ae", @"/Views/Home/Login.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24024fe4540337c1b6ce5608981a3f5afc0b5569", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Login : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("../../index3.html"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "E:\Proyectos\Plantilla Administrativa NetCore 3\PlantillaAdminNetcore3\PlantillaAdminNetcore3\Views\Home\Login.cshtml"
  
    ViewData["Title"] = "Login";
    Layout = "~/Areas/administracion/Views/Shared/IniciarSesion_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div class=""login-box"">
    <div class=""login-logo"">
        <a href=""../../index2.html""><b>Sistema </b>Estudiantir</a>
    </div>
    <!-- /.login-logo -->
    <div class=""card"">
        <div class=""card-body login-card-body"">
            <p class=""login-box-msg"">Inicia Sesion</p>

            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "dc1c958ab8e1cf91a72db2e0a9f9c6d22ca356ae4674", async() => {
                WriteLiteral(@"
                <div class=""input-group mb-3"">
                    <input type=""email"" class=""form-control"" placeholder=""Email"">
                    <div class=""input-group-append"">
                        <div class=""input-group-text"">
                            <span class=""fas fa-envelope""></span>
                        </div>
                    </div>
                </div>
                <div class=""input-group mb-3"">
                    <input type=""password"" class=""form-control"" placeholder=""Contraseña"">
                    <div class=""input-group-append"">
                        <div class=""input-group-text"">
                            <span class=""fas fa-lock""></span>
                        </div>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-8"">
                        <div class=""icheck-primary"">
                            <input type=""checkbox"" id=""remember"">
                            <label fo");
                WriteLiteral(@"r=""remember"">
                                Recordarme
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class=""col-4"">
                        <button type=""submit"" class=""btn btn-primary btn-block"">Iniciar Sesion</button>
                    </div>
                    <!-- /.col -->
                </div>
            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

            <!--<div class=""social-auth-links text-center mb-3"">
                <p>- OR -</p>
                <a href=""#"" class=""btn btn-block btn-primary"">
                    <i class=""fab fa-facebook mr-2""></i> Sign in using Facebook
                </a>
                <a href=""#"" class=""btn btn-block btn-danger"">
                    <i class=""fab fa-google-plus mr-2""></i> Sign in using Google+
                </a>
            </div>
            <!-- /.social-auth-links -->

            <p class=""mb-0"">
                <a href=""register.html"" class=""text-center"">Register a new membership</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
